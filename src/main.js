import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faFacebookSquare, faFacebookMessenger, faTwitter, faGithub, faLine, faApple, faAndroid } from '@fortawesome/free-brands-svg-icons'
import { faUserCircle, faHeart } from '@fortawesome/free-solid-svg-icons'
import { faClock, faFolder } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import App from './App.vue'
import 'velocity-animate'
import './assets/scss/main.scss'

library.add(
  faUserCircle,
  faHeart,
  faFacebookSquare,
  faFacebookMessenger,
  faTwitter,
  faGithub,
  faLine,
  faApple,
  faAndroid,
  faClock,
  faFolder
)

Vue.config.productionTip = false

Vue.component('blog-fa-icon', FontAwesomeIcon)

new Vue({
  render: h => h(App)
}).$mount('#app')
